package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge{
    ArrayList<FridgeItem> itemList = new ArrayList<>();
    int maxSize = 20;

    @Override
    public int nItemsInFridge() {
        return itemList.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(itemList.size() < 20){
            itemList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(itemList.contains(item)){
            itemList.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        itemList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(FridgeItem item : itemList){
            if(item.hasExpired()){
                expiredFood.add(item);
            }
        }
        itemList.removeAll(expiredFood);
        return expiredFood;
    }

}
